import React, {Component, Fragment} from 'react'
class Forecast extends Component {
  render() {
    var unixTime = Date.parse(this.props.forecast.dt_txt)
    var date = new Date(unixTime*1)
    return(
      <Fragment >
        <div className='flex-item'>  
          <p>{date.getHours()}:00 | {date.getDate()}  {date.toLocaleString('default', { month: 'short'})} </p>
          <p>{Math.floor(this.props.forecast.main.temp)}&deg;C | {this.props.forecast.main.humidity} % </p> <p> {this.props.forecast.weather[0].description} </p>
        </div>
      </Fragment>
    )
  }
}
export default Forecast
