import {LOCATION_PENDING, LOCATION_SUCCESS, LOCATION_NOT_AVAILABLE, LOCATION_NOT_ENABLED} from '../constants'

export function locationPending() {
  return {
     type: LOCATION_PENDING,
  }
}

export function locationSuccess(coords) {
  return {
    type: LOCATION_SUCCESS,
    coords: coords,
  }
}

export function locationNotAvailable() {
  return {
    type: LOCATION_NOT_AVAILABLE,
  }
}
export function locationNotEnabled() {
  return {
    type: LOCATION_NOT_ENABLED,
  }
}
