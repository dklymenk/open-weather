import {applyMiddleware, createStore, compose} from 'redux'
import rootReducer from './reducers/index'
import thunk from 'redux-thunk'
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose
const configureStore = preloadedState => (
    createStore(
        rootReducer,
        preloadedState,
        composeEnhancers(
          applyMiddleware(thunk)
        )
    )
)

const store = configureStore({})

export default store

