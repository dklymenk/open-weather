import {FETCH_WEATHER_PENDING, FETCH_WEATHER_SUCCESS, FETCH_WEATHER_ERROR} from '../constants'

const WEATHER_DATA = {
  pending: false,
  data: {},
  error: null,
}

export default function weatherData (state = WEATHER_DATA, action) {
  switch (action.type) {
    case FETCH_WEATHER_PENDING:
      return {
        ...state,
        pending: true,
      }
    case FETCH_WEATHER_SUCCESS:
      return {
        ...state, 
        pending: false, 
        data: action.weather,
      }
    case FETCH_WEATHER_ERROR:
      return {
        ...state,
        pending: false,
        error: action.error,
      }
    default:
      return state
  }
}

export const getWeather = state => state.weatherData.data
export const getWeatherPending = state => state.weatherData.pending
export const getWeatherError = state => state.weatherData.error

