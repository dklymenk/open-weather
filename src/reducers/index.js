import {combineReducers} from 'redux'
import weatherData from './weatherData'
import locationData from './locationData'

const rootReducer = combineReducers({weatherData, locationData})

export default rootReducer
