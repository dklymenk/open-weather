import {LOCATION_PENDING, LOCATION_SUCCESS, LOCATION_NOT_AVAILABLE, LOCATION_NOT_ENABLED} from '../constants'

const LOCATION_DATA = {
  pending: false,
  data: null,
  lat: null,
  lon: null,
  isAvailable: null,
  isEnabled: null,
}

export default function locationData (state = LOCATION_DATA, action) {
  switch (action.type) {
    case LOCATION_PENDING:
      return {
        ...state,
        pending: true,
      }
    case LOCATION_SUCCESS:
      return {
        ...state, 
        pending: false, 
        lat: action.coords.latitude,
        lon: action.coords.longitude,
        data: action.coords,
        isAvailable: true,
        isEnabled: true,
      }
    case LOCATION_NOT_AVAILABLE:
      return {
        ...state,
        pending: false,
        isAvailable: false,
      }
    case LOCATION_NOT_ENABLED:
      return {
        ...state,
        pending: false,
        isAvailable: true,
        isEnabled: false,
      }
    default:
      return state
  }
}

export const getLocation = state => ({lat: state.locationData.lat, lon: state.locationData.lon})
export const getLocationPending = state => state.locationData.pending
export const getLocationAvailability = state => state.locationData.isAvailable
export const getLocationStatus = state => state.locationData.isEnabled

