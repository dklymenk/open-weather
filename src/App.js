import React from 'react'
import Titles from './Titles'
import Form from './Form'
import Weather from './Weather'
import Location from './Location'

class App extends React.Component{
  render(){
    return (
      <div>
        <div align='center'>
          <header>
            <Form />
            <div className='location'>
              <Location />
            </div>
          </header>
        </div>
        <Titles />
        <Weather />
      </div>
    )
  }
}

export default App;
