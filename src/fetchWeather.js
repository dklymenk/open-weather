import {fetchWeatherPending, fetchWeatherSuccess, fetchWeatherError} from './actions/actionGetWeather'

function handleErrors (response) {
  if (!response.ok) {
    throw (response.status)
  }
  return response;
}

function fetchWeather(fetchBy,param) {
  return dispatch => {
    dispatch(fetchWeatherPending());
    const Api_Key = '89eef49321863f7fd1288163f8dc2234'
    let fetchString
    switch (fetchBy) {
      case 'CITY':
        fetchString = `https://api.openweathermap.org/data/2.5/forecast?q=${param}&appid=${Api_Key}&units=metric`;
        break
      case 'COORDS':
        fetchString = `https://api.openweathermap.org/data/2.5/forecast?lat=${param.lat}&lon=${param.lon}&appid=${Api_Key}&units=metric`;
        break
      default:
        fetchString = `https://api.openweathermap.org/data/2.5/forecast?q=kiev&appid=${Api_Key}&units=metric`
    }
    fetch(fetchString)
    .then(handleErrors)
    .then(res => res.json())
    .then(res => {
      dispatch(fetchWeatherSuccess(res))
      return res;
    })
    .catch((error) => {
        dispatch(fetchWeatherError(error));
    })
  }
}

export default fetchWeather;
