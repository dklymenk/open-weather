import React, {Component} from 'react'
import {connect} from 'react-redux'
import fetchWeather from './fetchWeather'
import {bindActionCreators} from 'redux'
import Autocomplete from 'react-autocomplete'
import cities from './cities'

class Form extends Component {
  constructor (props) {
    super(props)
    this.state={
      value: ''
    }
  }

  submitHandler = (e) => {
    e.preventDefault()
    const {fetchWeather} = this.props
    fetchWeather('CITY',e.target.elements.city.value) 
  }

  render (){
    let arr = []
    cities.forEach((element,index)=>arr.push({key:index,label:element}))
    return (
      <div className='search-bar'>
        <form className='form-search' onSubmit={this.submitHandler}>
          <Autocomplete
            getItemValue={(item) => item.label}
            items={arr}
            inputProps={{className: "input-search", type: "text", name: "city", placeholder: "City..."}}
            renderItem={(item, isHighlighted) =>
              <div 
                key = {item.key}
                style={{ background: isHighlighted ? 'lightgray' : 'white' }}>
                {item.label}
              </div>
            }
            shouldItemRender={(item, value) => value.length>1&&item.label.toLowerCase().startsWith(value.toLowerCase())}
            wrapperStyle = {{display: 'table-cell', width: '100%'}}
            value={this.state.value}
            onChange={(e) => this.setState ({value: e.target.value})}
            onSelect={(value) => this.setState({value})}
          />
          <button className='button-go'>GO</button>
        </form>
      </div>
    )
  }
}

const mapDispatchToProps = dispatch => bindActionCreators({
  fetchWeather: fetchWeather
}, dispatch)

export default connect(
  null,
  mapDispatchToProps
)(Form)
