import React, {Component} from 'react'
import Forecast from './Forecast'
import {connect} from 'react-redux'
import fetchWeather from './fetchWeather'
import {getWeatherError, getWeather, getWeatherPending} from './reducers/weatherData.js'
import {getLocation, getLocationPending, getLocationStatus} from './reducers/locationData.js'
import {bindActionCreators} from 'redux'

class Weather extends Component{
  state = {}
  
  static getDerivedStateFromProps(props){
    const {locationStatus,locationPending,locationData,fetchWeather,weatherData,pending, error} = props
    if (!error&&locationStatus&&!locationPending&&locationData&&!weatherData.list&&!pending){
      fetchWeather('COORDS', {lat: locationData.lat, lon: locationData.lon})
    }
    return null
  }
  renderDay(dayData){
    return (
      <div className='day' key={dayData[0].dt}>
        {dayData.map((element,index) => <Forecast key={element.dt} forecast={element}/>)}
        <hr width='100%'/>
      </div>
    )
  }

  render(){
    const {pending,weatherData, error} = this.props
    //var forecast = weatherData.list?weatherData.list.map((element,index) => <Forecast key={index} index={index} forecast={element}/>):[]
    var weatherData2 = {}
    let day = 0
    for (var hour in weatherData.list) {
      var date = new Date(weatherData.list[hour].dt*1000)  
      date.getHours()===3&&day++
      weatherData2 = {
        ...weatherData2,
        [day]: weatherData2[day]?weatherData2[day]:[],
      }
      weatherData2[day].push(weatherData.list[hour])
    }
    
    let forecast2 =[]
    for (var d in weatherData2){
      forecast2.push(this.renderDay(weatherData2[d]))
    }
    return(
      <div>  
      <center>
        {pending && <h2> Loading... </h2>} 
        {weatherData.city && <p>Location: { weatherData.city.name},   {weatherData.city.country}</p>}
        {weatherData.list && <p>Temperature: {Math.floor(weatherData.list[0].main.temp)}&deg;C </p>}
        {weatherData.list && <p>Humidity: {weatherData.list[0].main.humidity} %</p>}
        {weatherData.list && <p>Conditions:  {weatherData.list[0].weather[0].description}</p>}
      </center>
      <center>
      <div className="forecast">
        <div className="flex-container">
          {/*forecast*/}
          {forecast2}       
        </div>
      </div>
      </center>
        {error && <p>{error}</p>} 
      </div>
    )
  }
  

}
const mapStateToProps = state => ({
  error: getWeatherError(state),
  weatherData: getWeather(state),
  pending: getWeatherPending(state),
  locationData: getLocation(state),
  locationPending: getLocationPending(state),
  locationStatus: getLocationStatus(state),
})

const mapDispatchToProps = dispatch => bindActionCreators({
  fetchWeather: fetchWeather
}, dispatch)

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Weather);
