import React,{Component} from "react"
import {geolocated} from "react-geolocated"
import {connect} from 'react-redux'
import {bindActionCreators} from 'redux'
import {locationPending, locationSuccess, locationNotAvailable, locationNotEnabled} from './actions/actionLocation'

class Location extends Component {
  render(){
    const {locationPending, locationSuccess, locationNotAvailable, locationNotEnabled} = this.props
    if (!this.props.isGeolocationAvailable) {
      locationNotAvailable()
      return <div>Your browser does not support Geolocation</div>
    } else if (!this.props.isGeolocationEnabled) {
      locationNotEnabled()
      return <div>Geolocation is not enabled</div>
    } else if (this.props.coords) {
      locationSuccess(this.props.coords)
      return <div>Location OK</div>
    } else {
      locationPending()
      return <div>Getting the location data &hellip; </div>
    }
  }
}

const mapDispatchToProps = dispatch => bindActionCreators({
  locationPending: locationPending,
  locationSuccess: locationSuccess,
  locationNotAvailable: locationNotAvailable,
  locationNotEnabled: locationNotEnabled,
}, dispatch)

export default geolocated({
  positionOptions: {
    enableHighAccuracy: false,
  },
  userDecisionTimeout: 5000,
})(connect(null,mapDispatchToProps)(Location))
